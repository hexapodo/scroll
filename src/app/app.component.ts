import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  n = 50;
  verBoton = false;
  parrafos: string[] = [];

  ngOnInit(): void {
    this.parrafos = new Array(this.n).fill('Parrafo ');
    this.parrafos = this.parrafos.map((p, i) => p += '' + ++i);
  }

  @HostListener("window:scroll", ['$event'])
  manejaVisibilidadBoton($event:Event){
    let scrollOffset = ($event.target as HTMLElement).children[0].scrollTop;
    this.verBoton = scrollOffset > 200;
  }

}
